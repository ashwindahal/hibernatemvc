package com.cerotid.model;

public enum AccountType {
	CHECKING, SAVING, BUSINESS_CHECKING
}
