package com.cerotid.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "customer")
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "first_name", nullable = false)
	private String firstName;

	@Column(name = "last_name", nullable = false)
	private String lastName;

	@Column(name = "ssn", nullable = false)
	private String ssn;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="addressid")
    private Address address;
 
	public Customer() {

	}

	public Customer(String firstName, String lastName, String ssn) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.ssn = ssn;
	}

	public Customer(int id, String firstName, String lastName, String ssn) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.ssn = ssn;
	}

	public Customer(String firstName, String lastName, String ssn, Address address) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.ssn = ssn;
		this.address = address;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", ssn=" + ssn
				+ ", address=" + address + "]";
	}



	/*
	 * @Id
	 * 
	 * @GeneratedValue(strategy = GenerationType.IDENTITY) private int id;
	 * 
	 * @Size(min = 3, max = 50)
	 * 
	 * @Column(name = "firstName", nullable = false) private String firstName;
	 * 
	 * @Size(min = 3, max = 50)
	 * 
	 * @Column(name = "lastName", nullable = false) private String lastName;
	 * 
	 * @NotEmpty
	 * 
	 * @Column(name = "ssn", unique = true, nullable = false) private String ssn;
	 * 
	 * @OneToOne(cascade = { CascadeType.ALL }) private Address address;
	 * 
	 * @ManyToOne(cascade = { CascadeType.ALL }) private Account accounts;
	 * 
	 * public Customer() {
	 * 
	 * }
	 * 
	 * public Customer(int id, @Size(min = 3, max = 50) String firstName, @Size(min
	 * = 3, max = 50) String lastName,
	 * 
	 * @NotEmpty String ssn) { super(); this.id = id; this.firstName = firstName;
	 * this.lastName = lastName; this.ssn = ssn; }
	 * 
	 * public Customer(@Size(min = 3, max = 50) String firstName, @Size(min = 3, max
	 * = 50) String lastName,
	 * 
	 * @NotEmpty String ssn) { super(); this.firstName = firstName; this.lastName =
	 * lastName; this.ssn = ssn; }
	 * 
	 * public int getId() { return id; }
	 * 
	 * public void setId(int id) { this.id = id; }
	 * 
	 * public String getFirstName() { return firstName; }
	 * 
	 * public void setFirstName(String firstName) { this.firstName = firstName; }
	 * 
	 * public String getLastName() { return lastName; }
	 * 
	 * public void setLastName(String lastName) { this.lastName = lastName; }
	 * 
	 * public String getSsn() { return ssn; }
	 * 
	 * public void setSsn(String ssn) { this.ssn = ssn; }
	 * 
	 * public Address getAddress() { return address; }
	 * 
	 * public void setAddress(Address address) { this.address = address; }
	 * 
	 * public Account getAccounts() { return accounts; }
	 * 
	 * public void setAccounts(Account accounts) { this.accounts = accounts; }
	 * 
	 * @Override public String toString() { return "Customer [id=" + id +
	 * ", firstName=" + firstName + ", lastName=" + lastName + ", ssn=" + ssn +
	 * ", address=" + address + ", accounts=" + accounts + "]"; }
	 */

}
