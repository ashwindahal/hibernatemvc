package com.cerotid.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

//does not even get to this entity 

@Entity
@Table(name = "address")
public class Address {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int addressid;

	@Column(name = "addressLine", nullable = false)
	private String addressLine;

	@Column(name = "city", nullable = false)
	private String city;

	@Column(name = "zipCode", nullable = false)
	private String zipCode;

	@Column(name = "stateCode", nullable = false)
	private String stateCode;

	public Address() {

	}

	public Address(int addressid, String addressLine, String city, String zipCode, String stateCode) {
		super();
		this.addressid = addressid;
		this.addressLine = addressLine;
		this.city = city;
		this.zipCode = zipCode;
		this.stateCode = stateCode;

	}

	public Address(String addressLine, String city, String zipCode, String stateCode) {
		super();
		this.addressLine = addressLine;
		this.city = city;
		this.zipCode = zipCode;
		this.stateCode = stateCode;

	}

	public int getAddressid() {
		return addressid;
	}

	public void setAddressid(int addressid) {
		this.addressid = addressid;
	}

	public String getAddressLine() {
		return addressLine;
	}

	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	@Override
	public String toString() {
		return "Address [addressid=" + addressid + ", addressLine=" + addressLine + ", city=" + city + ", zipCode="
				+ zipCode + ", stateCode=" + stateCode + "]";
	}

	/*
	 * @Id
	 * 
	 * @GeneratedValue(strategy = GenerationType.IDENTITY) private int addressid;
	 * 
	 * @Column(name = "addressLine") private String addressLine;
	 * 
	 * @Column(name = "city") private String city;
	 * 
	 * // validation (Either XXXXX or XXXXX-XXXX)
	 * 
	 * @Length(min = 5, max = 5)
	 * 
	 * @Column(name = "zipCode") private String zipCode;
	 * 
	 * // Make sure all Caps; Max/Min Length 2.
	 * 
	 * @Length(min = 2, max = 2)
	 * 
	 * @Pattern(regexp =
	 * "^(?-i:A[LKSZRAEP]|C[AOT]|D[EC]|F[LM]|G[AU]|HI|I[ADLN]|K[SY]|LA|M[ADEHINOPST]|N[CDEHJMVY]|O[HKR]|P[ARW]|RI|S[CD]|T[NX]|UT|V[AIT]|W[AIVY])$",
	 * message = "Invalid Sate Code") private String stateCode;
	 * 
	 * @OneToOne(fetch = FetchType.LAZY)
	 * 
	 * @JoinColumn(name = "customer_id") private Customer customerId;
	 * 
	 * public Address() {
	 * 
	 * }
	 * 
	 * public Address(int addressid, String addressLine, String city, @Length(min =
	 * 5, max = 5) String zipCode,
	 * 
	 * @Length(min = 2, max = 2) @Pattern(regexp =
	 * "^(?-i:A[LKSZRAEP]|C[AOT]|D[EC]|F[LM]|G[AU]|HI|I[ADLN]|K[SY]|LA|M[ADEHINOPST]|N[CDEHJMVY]|O[HKR]|P[ARW]|RI|S[CD]|T[NX]|UT|V[AIT]|W[AIVY])$",
	 * message = "Invalid Sate Code") String stateCode) { super(); this.addressid =
	 * addressid; this.addressLine = addressLine; this.city = city; this.zipCode =
	 * zipCode; this.stateCode = stateCode; }
	 * 
	 * public Address(String addressLine, String city, @Length(min = 5, max = 5)
	 * String zipCode,
	 * 
	 * @Length(min = 2, max = 2) @Pattern(regexp =
	 * "^(?-i:A[LKSZRAEP]|C[AOT]|D[EC]|F[LM]|G[AU]|HI|I[ADLN]|K[SY]|LA|M[ADEHINOPST]|N[CDEHJMVY]|O[HKR]|P[ARW]|RI|S[CD]|T[NX]|UT|V[AIT]|W[AIVY])$",
	 * message = "Invalid Sate Code") String stateCode) { super(); this.addressLine
	 * = addressLine; this.city = city; this.zipCode = zipCode; this.stateCode =
	 * stateCode; }
	 * 
	 * public int getAddress_id() { return addressid; }
	 * 
	 * public void setAddress_id(int address_id) { this.addressid = address_id; }
	 * 
	 * public String getAddressLine() { return addressLine; }
	 * 
	 * public void setAddressLine(String addressLine) { this.addressLine =
	 * addressLine; }
	 * 
	 * public String getCity() { return city; }
	 * 
	 * public void setCity(String city) { this.city = city; }
	 * 
	 * public String getZipCode() { return zipCode; }
	 * 
	 * public void setZipCode(String zipCode) { this.zipCode = zipCode; }
	 * 
	 * public String getStateCode() { return stateCode; }
	 * 
	 * public void setStateCode(String stateCode) { this.stateCode = stateCode; }
	 * 
	 * @Override public String toString() { return "Address [address_id=" +
	 * addressid + ", addressLine=" + addressLine + ", city=" + city + ", zipCode="
	 * + zipCode + ", stateCode=" + stateCode + "]"; }
	 */
}
