package com.cerotid.model;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/*@Entity
@Table(name = "account")*/
public class Account {
	/*
	 * @Id
	 * 
	 * @GeneratedValue(strategy = GenerationType.IDENTITY) private int accountid;
	 * 
	 * // ask how to use this // @Enumerated(EnumType.STRING)
	 * 
	 * @Column(name = "accountType") private AccountType accountType;
	 * 
	 * @Basic
	 * 
	 * @Temporal(TemporalType.TIMESTAMP)
	 * 
	 * @Column(name = "account_open_date") private Date accountOpenDate;
	 * 
	 * @Basic
	 * 
	 * @Temporal(TemporalType.TIMESTAMP)
	 * 
	 * @Column(name = "account_close_date") private Date accountCloseDate;
	 * 
	 * @Column(name = "amount", precision = 19, scale = 4) private double amount;
	 * 
	 * 
	 * @ManyToOne
	 * 
	 * @JoinColumn(name = "customer_id") private Customer customerId;
	 * 
	 * public Account() {
	 * 
	 * }
	 * 
	 * public Account(int accountd, AccountType accountType, Date accountOpenDate,
	 * Date accountCloseDate, double amount) {
	 * 
	 * this.accountid = accountd; this.accountType = accountType;
	 * this.accountOpenDate = accountOpenDate; this.accountCloseDate =
	 * accountCloseDate; this.amount = amount; }
	 * 
	 * public Account(AccountType accountType, Date accountOpenDate, Date
	 * accountCloseDate, double amount) { super(); this.accountType = accountType;
	 * this.accountOpenDate = accountOpenDate; this.accountCloseDate =
	 * accountCloseDate; this.amount = amount; }
	 * 
	 * public AccountType getAccountType() { return accountType; }
	 * 
	 * public void setAccountType(AccountType accountType) { this.accountType =
	 * accountType; }
	 * 
	 * public Date getAccountOpenDate() { return accountOpenDate; }
	 * 
	 * public void setAccountOpenDate(Date accountOpenDate) { this.accountOpenDate =
	 * accountOpenDate; }
	 * 
	 * public Date getAccountCloseDate() { return accountCloseDate; }
	 * 
	 * public void setAccountCloseDate(Date accountCloseDate) {
	 * this.accountCloseDate = accountCloseDate; }
	 * 
	 * public double getAmount() { return amount; }
	 * 
	 * public void setAmount(double amount) { this.amount = amount; }
	 * 
	 * @Override public String toString() { return "Account [accountd=" + accountid
	 * + ", accountType=" + accountType + ", accountOpenDate=" + accountOpenDate +
	 * ", accountCloseDate=" + accountCloseDate + ", amount=" + amount +
	 * ", customerId=" + customerId + "]"; }
	 */
}
