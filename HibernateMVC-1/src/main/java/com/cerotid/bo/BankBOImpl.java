package com.cerotid.bo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cerotid.dao.BankDAO;
import com.cerotid.model.Account;
import com.cerotid.model.Customer;

@Component
public class BankBOImpl implements BankBO {

	@Autowired
	private BankDAO bankDAO;

	@Override
	public void openAccount(Customer customer, Account account) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void depositMoneyInCustomerAccount(Customer customer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Customer getCustomerInfo(String ssn) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void printBankStatus() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Customer> getCustomersByState(String stateCode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean checkSsn(String ssn) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void addCustomer(Customer customer) {
		bankDAO.addCustomer(customer);
	}

	@Override
	public List<Customer> retrieveCustomers() {
		// TODO Auto-generated method stub
		return bankDAO.retrieveCustomers(); 
	}

	@Override
	public void editCustomerInfo(Customer customer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteCustomer(Customer customer) {
		// TODO Auto-generated method stub
		
	}

}
