package com.cerotid.bo;

import java.util.List;

import com.cerotid.model.Account;
import com.cerotid.model.Customer;

public interface BankBO {

	public void openAccount(Customer customer, Account account);

	public void depositMoneyInCustomerAccount(Customer customer);

	public Customer getCustomerInfo(String ssn);

	public void printBankStatus();

	public List<Customer> getCustomersByState(String stateCode);// opens a Pandora box of COllection Framework //
																// Understanding

	public boolean checkSsn(String ssn);

	public void addCustomer(Customer customer);

	public List<Customer> retrieveCustomers();

	public void editCustomerInfo(Customer customer);

	public void deleteCustomer(Customer customer);

}
