package com.cerotid.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cerotid.bo.BankBO;
import com.cerotid.model.Address;
import com.cerotid.model.Customer;

@Controller
public class CustomerController {

	@Autowired
	@Qualifier("bankBOImpl")
	private BankBO bankBO;

	@RequestMapping("/homepage")
	public String homePage(ModelMap model) {
		return "homepage";
	}

	@RequestMapping("/add-customer-form")
	public String addCustomer(ModelMap model) {

		return "add-customer-form";
	}

	@RequestMapping(value = "/addCustomer", method = RequestMethod.POST)
	public String addCustomer(Customer customer, Address address, ModelMap model) {

		model.addAttribute("firstName", customer.getFirstName());
		model.addAttribute("lastName", customer.getLastName());
		model.addAttribute("ssn", customer.getSsn());

		model.addAttribute("addressLine", address.getAddressLine());
		model.addAttribute("city", address.getCity());
		model.addAttribute("zipCode", address.getZipCode());
		model.addAttribute("stateCode", address.getStateCode());

		// setting the customers address and placing the customer in a list
		customer.setAddress(address);
		bankBO.addCustomer(customer);

		System.out.println(customer);

		// adding customerList to the model so we can pass it to the view
		 model.addAttribute("customerList", bankBO.retrieveCustomers());

		return "customer-display-page";

	}

}
