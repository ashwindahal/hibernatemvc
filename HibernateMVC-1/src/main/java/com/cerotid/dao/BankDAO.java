package com.cerotid.dao;

import java.util.List;

import com.cerotid.model.Account;
import com.cerotid.model.Customer;

public interface BankDAO {

	public void addCustomer(Customer customer);

	public List<Customer> getCustomers();

	public void openAccount(Customer customer, Account account);

	public void depositMoneyInCustomerAccount(Customer customer);

	public Customer getCustomerInfo(String ssn);

	public void printBankStatus();

	public List<Customer> getCustomersByState(String stateCode);// opens a Pandora box of COllection Framework //
																// Understanding

	public boolean checkSsn(String ssn);

	public List<Customer> retrieveCustomers();

	public void editCustomerInfo(Customer customer);

	public void deleteCustomer(Customer customer);

}
