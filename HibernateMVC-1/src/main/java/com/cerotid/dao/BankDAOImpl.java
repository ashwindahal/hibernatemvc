package com.cerotid.dao;

import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cerotid.model.Account;
import com.cerotid.model.Address;
import com.cerotid.model.Customer;

@Repository
public class BankDAOImpl implements BankDAO {

	@Autowired
	private EntityManagerFactory entityManagerFactory;

	@Override
	@Transactional
	public void addCustomer(Customer customer) {
		Transaction transaction = null;
		try (Session session = entityManagerFactory.unwrap(SessionFactory.class).openSession()) {
			// start a transaction
			transaction = session.beginTransaction();
			// save the student object
			session.save(customer);
			// commit transaction
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
				System.out.println("SOME PROBLEM HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			}
			e.printStackTrace();
		}
	}

	@Override
	public List<Customer> getCustomers() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void openAccount(Customer customer, Account account) {
		// TODO Auto-generated method stub

	}

	@Override
	public void depositMoneyInCustomerAccount(Customer customer) {
		// TODO Auto-generated method stub

	}

	@Override
	public Customer getCustomerInfo(String ssn) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void printBankStatus() {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Customer> getCustomersByState(String stateCode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean checkSsn(String ssn) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Customer> retrieveCustomers() {
		Session session = entityManagerFactory.unwrap(SessionFactory.class).openSession();
		List<Customer> customer = (List<Customer>) session.createQuery("SELECT firstName FROM Customer c", Customer.class).getResultList();
		System.out.println(customer);
		return customer;
	}

	@Override
	public void editCustomerInfo(Customer customer) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteCustomer(Customer customer) {
		// TODO Auto-generated method stub

	}

}
