package com.cerotid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
public class HibernateMvc1Application {

	public static void main(String[] args) {
		SpringApplication.run(HibernateMvc1Application.class, args);
	}

}
