<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<html>

<head>
<title>Add Customer</title>

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/page-style.css" />

</head>
<body>
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />

	<center>
		<img src="<c:url value='/images/logo.png'/>" />
	</center>
	<hr />

	<h2>Add Customer</h2>


	<form:form method="POST" action="${contextPath}/addCustomer">
		<input type="hidden" name="command" value="ADD">

		<table>

			<tr>
				<td>Customer First name:<br> <input type="text"
					name="firstName" /></td>
			</tr>

			<tr>
				<td>Customer Last Name:<br> <input type="text"
					name="lastName" />

				</td>
			</tr>

			<tr>
				<td>Customer SSN (9 digit):<br> <input type="text"
					id="fieldSsn" name="ssn" placeholder="555-55-5555"
					pattern="\d{3}-?\d{2}-?\d{4}" maxlength="9">
				</td>
			</tr>

			<tr>
				<td>Customer Street Address:<br> <input type="text"
					name="addressLine" />
				</td>
			</tr>

			<tr>
				<td>Customer City:<br> <input type="text" name="city" />
				</td>
			</tr>

			<tr>
				<td>Customer Zip code:<br> <input type="text"
					name="zipCode" />
				</td>
			</tr>

			<tr>
				<td>Customer State code:<br> <input type="text"
					name="stateCode" />
				</td>
			</tr>

			<tr>
				<td><input type="submit" value="Submit" /></td>
			</tr>

		</table>
	</form:form>
	<!-- Ask if this is the best way to do this -->
	<a href="/cerotid/homepage" Title="Link to Homepage"><h2
			align="center">Return to HomePage</h2> </a>

	<br />
	<br />



</body>

</html>